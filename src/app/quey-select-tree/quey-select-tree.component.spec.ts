import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueySelectTreeComponent } from './quey-select-tree.component';

describe('QueySelectTreeComponent', () => {
  let component: QueySelectTreeComponent;
  let fixture: ComponentFixture<QueySelectTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueySelectTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueySelectTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
