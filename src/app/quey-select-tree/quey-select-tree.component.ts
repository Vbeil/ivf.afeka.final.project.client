import {
  Component,
  Injectable,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';

/**
 * Node for select item
 */
export class SelectItemNode {
  children: SelectItemNode[];
  item: string;
}

/** Flat select item node with expandable and level information */
export class SelectItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for select list data.
 */
var TREE_DATA: any = {};

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a select item or a table.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<SelectItemNode[]>([]);

  get data(): SelectItemNode[] {
    return this.dataChange.value;
  }

  constructor() {
    this.initialize();
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `SelectItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `SelectItemNode`.
   */
  buildFileTree(obj: { [key: string]: any }, level: number): SelectItemNode[] {
    return Object.keys(obj).reduce<SelectItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new SelectItemNode();
      node.item = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.item = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }
}

@Component({
  selector: 'app-quey-select-tree',
  templateUrl: './quey-select-tree.component.html',
  styleUrls: ['./quey-select-tree.component.css'],
  providers: [ChecklistDatabase],
})
export class QueySelectTreeComponent implements OnInit {
  ngOnInit(): void {}

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<SelectItemFlatNode, SelectItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<SelectItemNode, SelectItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: SelectItemFlatNode | null = null;

  treeControl: FlatTreeControl<SelectItemFlatNode>;

  treeFlattener: MatTreeFlattener<SelectItemNode, SelectItemFlatNode>;

  dataSource: MatTreeFlatDataSource<SelectItemNode, SelectItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<SelectItemFlatNode>(
    true /* multiple */
  );

  private _database: ChecklistDatabase;

  constructor() {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      this.getChildren
    );
    this.treeControl = new FlatTreeControl<SelectItemFlatNode>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new MatTreeFlatDataSource(
      this.treeControl,
      this.treeFlattener
    );
  }

  @Input() currentTableData: DbTableDataForSelect;
  @Output() tableDataChange = new EventEmitter<DbTableDataForSelect>();
  @Input() set tableData(v: DbTableDataForSelect) {
    this.currentTableData = v;
    var tableName: string = v.tableName;

    var jsonString = '{ "' + tableName + '": { ';

    for (var idx = 0; idx < v.fields.length; idx++) {
      if (idx < v.fields.length - 1)
        jsonString = jsonString + '"' + v.fields[idx].name + '": null, ';
      else jsonString = jsonString + '"' + v.fields[idx].name + '": null }}';
    }
    TREE_DATA = JSON.parse(jsonString);

    this._database = new ChecklistDatabase();
    this._database.dataChange.subscribe((data) => {
      this.dataSource.data = data;
    });

    this.tableDataChange.emit(v);
  }
  get tableData(): DbTableDataForSelect {
    return this.currentTableData;
  }

  @Input() currentSelectedQueryFilterFields: selectedQueryFilterFields[];
  @Output()
  selectedQueryFilterFieldsChange = new EventEmitter<
    selectedQueryFilterFields[]
  >();
  @Input() set selectedQueryFilterFields(v: selectedQueryFilterFields[]) {
    this.selectedQueryFilterFieldsChange.emit(v);
    this.currentSelectedQueryFilterFields = v;
  }
  get selectedQueryFilterFields(): selectedQueryFilterFields[] {
    return this.currentSelectedQueryFilterFields;
  }

  getLevel = (node: SelectItemFlatNode) => node.level;

  isExpandable = (node: SelectItemFlatNode) => node.expandable;

  getChildren = (node: SelectItemNode): SelectItemNode[] => node.children;

  hasChild = (_: number, _nodeData: SelectItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: SelectItemFlatNode) =>
    _nodeData.item === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: SelectItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode =
      existingNode && existingNode.item === node.item
        ? existingNode
        : new SelectItemFlatNode();
    flatNode.item = node.item;
    flatNode.level = level;
    flatNode.expandable = !!node.children?.length;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  };

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: SelectItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every((child) => {
        return this.checklistSelection.isSelected(child);
      });
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: SelectItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some((child) =>
      this.checklistSelection.isSelected(child)
    );
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the select item selection. Select/deselect all the descendants node */
  selectItemSelectionToggle(node: SelectItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.forEach((child) => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf select item selection. Check all the parents to see if they changed */
  selectLeafItemSelectionToggle(node: SelectItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: SelectItemFlatNode): void {
    let parent: SelectItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }

    this.selectedQueryFilterFields = [];
    this.checklistSelection.selected.forEach((v) => {
      if (v.level > 0) {
        this.selectedQueryFilterFields.push({
          tableName: this.currentTableData.tableName,
          fieldName: v.item.toString(),
        });
      }
    });
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: SelectItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every((child) => {
        return this.checklistSelection.isSelected(child);
      });
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: SelectItemFlatNode): SelectItemFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }
}

export interface DbTableField {
  name: string;
  type: string;
}

export interface DbTableDataForSelect {
  tableName: string;
  fields: DbTableField[];
}

export interface selectedQueryFilterFields {
  tableName: string;
  fieldName: string;
}
