import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ResearchService {
  authenticationState = new BehaviorSubject(false);
  constructor(private http: HttpClient) {}

  baseUrl: string = '/research/query';

  public sendQueryToServer(query: string): any {
    console.log(query);
    return this.http.post(this.baseUrl + '/json', {
      query,
    });
  }
}
