import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DbTableService {
  authenticationState = new BehaviorSubject(false);
  constructor(private http: HttpClient) {}

  baseUrl: string = '/tables';

  public getAllTablesName(): any {
    return this.http.get(this.baseUrl);
  }

  public getFieldsByTableName(tableName: string): any {
    return this.http.get(this.baseUrl + '/' + tableName);
  }

  public upload(data, tableName, isOverride: boolean): any {
    console.log(data);
    if (isOverride)
      return this.http.post('/dataUpload/' + tableName + '/override/', data);
    else return this.http.post('/dataUpload/' + tableName + '/add/', data);
  }

  public clearDatabase(): any {
    return this.http.delete('/dataUpload/clearDatabase');
  }
}
