import { TestBed } from '@angular/core/testing';

import { DbTableService } from './db-table.service';

describe('DbTableService', () => {
  let service: DbTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DbTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
