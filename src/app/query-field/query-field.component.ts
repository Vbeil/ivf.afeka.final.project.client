import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { DbTableService } from '../services/db-table.service';

@Component({
  selector: 'app-query-field',
  templateUrl: './query-field.component.html',
  styleUrls: ['./query-field.component.css'],
})
export class QueryFieldComponent implements OnInit {
  options: string[] = [];

  @Input() currentIsFirst: boolean;
  @Output() isFirstChange = new EventEmitter<boolean>();
  @Input() set isFirst(v: boolean) {
    this.currentIsFirst = v;
    this.isFirstChange.emit(v);
  }
  get isFirst(): boolean {
    return this.currentIsFirst;
  }

  @Input() currentTables: string[];
  @Output() tablesChange = new EventEmitter<string[]>();
  @Input() set tables(t: string[]) {
    this.currentTables = t;
    this.tablesChange.emit(t);
  }
  get tables(): string[] {
    return this.currentTables;
  }

  @Input() currentSelectedTable: string;
  @Output() selectedTableChange = new EventEmitter<string>();
  @Input() set selectedTable(t: string) {
    this.currentSelectedTable = t;
    this.selectedTableChange.emit(t);
    this.selectedField = undefined;
    this.option = undefined;
    this.val = undefined;
  }
  get selectedTable(): string {
    return this.currentSelectedTable;
  }

  @Input() currentFields: DbTableField[];
  @Output() fieldsChange = new EventEmitter<DbTableField[]>();
  @Input() set fields(f: DbTableField[]) {
    this.currentFields = f;
    this.fieldsChange.emit(f);
  }
  get fields(): DbTableField[] {
    return this.currentFields;
  }

  @Input() currentSelectedField: DbTableField;
  @Output() selectedFieldChange = new EventEmitter<DbTableField>();
  @Input() set selectedField(f: DbTableField) {
    this.currentSelectedField = f;
    this.selectedFieldChange.emit(f);
    this.options = [];
    if (f != undefined) {
      if (f.type.toLowerCase().includes('char')) {
        this.options.push('Like');
        this.options.push('In (separate by $)');
      } else if (
        f.type.toLowerCase().includes('bool') ||
        f.type.toLowerCase().includes('bit')
      ) {
        this.options.push('True');
        this.options.push('False');
      } else {
        this.options.push('Equals');
        this.options.push('Greater Than');
        this.options.push('Less Than');
      }
    }
  }
  get selectedField(): DbTableField {
    return this.currentSelectedField;
  }

  @Input() currentOperator: string;
  @Output() operatorChange = new EventEmitter<string>();
  @Input() set operator(o: string) {
    this.currentOperator = o;
    this.operatorChange.emit(o);
  }
  get operator(): string {
    return this.currentOperator;
  }

  @Input() currentOption: string;
  @Output() optionChange = new EventEmitter<string>();
  @Input() set option(o: string) {
    this.currentOption = o;
    this.optionChange.emit(o);
  }
  get option(): string {
    return this.currentOption;
  }

  @Input() currentVal: string;
  @Output() valChange = new EventEmitter<string>();
  @Input() set val(v: string) {
    this.currentVal = v;
    this.valChange.emit(v);
  }
  get val(): string {
    return this.currentVal;
  }

  @Input() currentFromTime: string;
  @Output() fromTimeChange = new EventEmitter<string>();
  @Input() set fromTime(v: string) {
    this.currentFromTime = v;
    this.fromTimeChange.emit(v);
  }
  get fromTime(): string {
    return this.currentFromTime;
  }

  @Input() currentToTime: string;
  @Output() toTimeChange = new EventEmitter<string>();
  @Input() set toTime(v: string) {
    this.currentToTime = v;
    this.toTimeChange.emit(v);
  }
  get toTime(): string {
    return this.currentToTime;
  }

  @Input() currentFromDate: string;
  @Output() fromDateChange = new EventEmitter<string>();
  @Input() set fromDate(v: string) {
    this.currentFromDate = v;
    this.fromDateChange.emit(v);
  }
  get fromDate(): string {
    return this.currentFromDate;
  }

  @Input() currentToDate: string;
  @Output() toDateChange = new EventEmitter<string>();
  @Input() set toDate(v: string) {
    this.currentToDate = v;
    this.toDateChange.emit(v);
  }
  get toDate(): string {
    return this.currentToDate;
  }

  @Output() tableChangeEvent = new EventEmitter<DbTableDataForSelect>();

  constructor(private dbTableService: DbTableService) {}

  ngOnInit(): void {}

  loadFields() {
    this.dbTableService
      .getFieldsByTableName(this.selectedTable)
      .subscribe((dbFields: DbTableField[]) => {
        console.log(dbFields);
        dbFields.splice(
          dbFields.findIndex((f) => f.name.toLowerCase() == 'row_id'),
          1
        );
        this.fields = dbFields;
        var fieldsNameAndType: DbTableField[] = [];
        this.fields.forEach((f) => {
          fieldsNameAndType.push({
            name: f.name,
            type: f.type,
          });
        });

        var tableData: DbTableDataForSelect = {
          tableName: this.selectedTable,
          fields: fieldsNameAndType,
        };

        this.tableChangeEvent.emit(tableData);
      });
  }
}

export interface DbTableField {
  name: string;
  type: string;
}

export interface DbTableDataForSelect {
  tableName: string;
  fields: DbTableField[];
}
