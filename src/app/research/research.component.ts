import {
  AfterViewInit,
  Component,
  ViewChild,
  ChangeDetectorRef,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { QueryFieldComponent } from '../query-field/query-field.component';
import { DbTableService } from '../services/db-table.service';
import { ResearchService } from '../services/research.service';
import { QueySelectTreeComponent } from '../quey-select-tree/quey-select-tree.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.css'],
})
export class ResearchComponent implements AfterViewInit {
  title: string = 'RESEARCH';

  queryFields: QueryFieldComponent[] = [];
  tables: string[] = [];

  querySelectFields: QueySelectTreeComponent[] = [];

  isLoadingResults = false;
  showTable = false;
  resultsLength = 0;

  displayedColumns: string[] = [];

  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private cdr: ChangeDetectorRef,
    private dbTableService: DbTableService,
    private researchService: ResearchService
  ) {}

  ngAfterViewInit() {
    this.dbTableService.getAllTablesName().subscribe((tables: any) => {
      this.tables = tables;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getResults() {
    if (!this.isLoadingResults) {
      var query = this.buildQuery();
      if (query != undefined && query.length > 0) {
        this.isLoadingResults = true;
        this.researchService
          .sendQueryToServer(query)
          .subscribe((dbData: any) => {
            this.displayedColumns = [];
            if (Array.isArray(dbData)) {
              dbData.forEach((d) => {
                Object.keys(d).forEach((key) => {
                  if (
                    !this.displayedColumns.includes(key) &&
                    key.toLowerCase() != 'row_id'
                  )
                    this.displayedColumns.push(key);
                });
              });
              this.resultsLength = dbData.length;
              this.showTable = true;
              setTimeout(() => {
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.dataSource.data = dbData;
                this.cdr.detectChanges();
              });
            } else {
              this.showTable = false;
              alert('No results were found');
            }

            this.isLoadingResults = false;
          });
      }
    }
  }

  buildQuery(): string {
    var query: string;
    if (this.validateQuery()) {
      var leadTable: QueryFieldComponent = this.queryFields.find(
        (qf) => qf.isFirst
      );

      //SELECT
      query = 'SELECT DISTINCT ';
      var selectAll: boolean = true;
      this.querySelectFields.forEach((qsf) => {
        if (
          qsf.selectedQueryFilterFields &&
          qsf.selectedQueryFilterFields.length > 0
        ) {
          selectAll = false;
          qsf.selectedQueryFilterFields.forEach((f) => {
            query =
              query +
              f.tableName.toLocaleLowerCase() +
              '.' +
              f.fieldName +
              ', ';
          });
        }
      });

      if (selectAll) {
        query = query + '*';
      } else {
        //remove last ,
        query = query.substring(0, query.lastIndexOf(','));
      }

      //FROM
      var leadTableName: string = leadTable.selectedTable;
      var tables: string[] = [];
      this.queryFields.forEach((qf) => {
        var currTable = qf.selectedTable;
        if (currTable != leadTableName && !tables.includes(currTable))
          tables.push(currTable);
      });

      query =
        query +
        '\nFROM ' +
        leadTableName +
        ' ' +
        leadTableName.toLocaleLowerCase();

      tables.forEach((t) => {
        query =
          query +
          '\nLEFT JOIN ' +
          t +
          ' ' +
          t.toLocaleLowerCase() +
          ' ON ' +
          t.toLocaleLowerCase() +
          '.PATIENT_ID = ' +
          leadTableName.toLocaleLowerCase() +
          '.PATIENT_ID';
      });

      //WHERE
      //lead table
      query = query + '\nWHERE (\n';
      query = this.queryBuilderWhereHelper(query, leadTable);
      //other tables
      this.queryFields.forEach((qf) => {
        if (
          !qf.isFirst &&
          (qf.selectedField != undefined ||
            qf.operator.includes('(') ||
            qf.operator.includes(')'))
        ) {
          if (qf.operator.toUpperCase().includes('EXISTS')) {
            query =
              query +
              '\n' +
              qf.operator.toUpperCase() +
              ' ( SELECT 1 FROM ' +
              qf.selectedTable.toLocaleLowerCase() +
              ' WHERE ' +
              qf.selectedTable.toLocaleLowerCase() +
              '.PATIENT_ID = ' +
              leadTableName.toLocaleLowerCase() +
              '.PATIENT_ID AND ';
            query = this.queryBuilderWhereHelper(query, qf);
            query = query + ')';
          } else {
            query = query + '\n' + qf.operator.toUpperCase() + ' ';
            query = this.queryBuilderWhereHelper(query, qf);
          }
        }
      });
    }
    let queryWhereClause = query.substring(query.indexOf('WHERE'));
    let addClosingBrackets: number = this.calcClosingBrackets(queryWhereClause);
    if (addClosingBrackets > -1) {
      let ClosingBrackets = ' ';
      for (let i = 0; i < addClosingBrackets; i++) {
        ClosingBrackets = ClosingBrackets + ')';
      }
      return query + ClosingBrackets;
    } else {
      alert('Invalid Query!');
      return '';
    }
  }

  calcClosingBrackets(queryWhereClause: string): number {
    let rv: number = 0;
    for (let i = 0; i < queryWhereClause.length; i++) {
      if (queryWhereClause.charAt(i).includes('(')) rv++;
      else if (queryWhereClause.charAt(i).includes(')')) rv--;
    }
    return rv > 0 ? rv : 0;
  }

  queryBuilderWhereHelper(query: string, qf: QueryFieldComponent): string {
    if (qf.selectedField == undefined) {
      query = query + '1 = 1';
    } else {
      if (qf.selectedField.type.toLowerCase().includes('char')) {
        query =
          query +
          'LOWER(' +
          qf.selectedTable.toLocaleLowerCase() +
          '.' +
          qf.selectedField.name +
          ')';
      } else {
        query =
          query +
          qf.selectedTable.toLocaleLowerCase() +
          '.' +
          qf.selectedField.name;
      }
      if (
        qf.selectedField.type.toLowerCase().includes('date') ||
        qf.selectedField.type.toLowerCase().includes('time')
      ) {
        if (
          qf.selectedField.type.toLowerCase().includes('date') ||
          qf.selectedField.type.toLowerCase().includes('timestamp')
        ) {
          query =
            query +
            ' BETWEEN ' +
            "'" +
            this.formatDateForDb(
              new Date(qf.fromDate).toLocaleDateString('en-GB')
            ) +
            "'" +
            ' AND ' +
            "'" +
            this.formatDateForDb(
              new Date(qf.toDate).toLocaleDateString('en-GB')
            ) +
            "'";
        } else {
          query =
            query +
            ' BETWEEN ' +
            "'" +
            qf.fromTime +
            "'" +
            ' AND ' +
            "'" +
            qf.toTime +
            "'";
        }
      } else if (
        qf.selectedField.type.toLowerCase().includes('bool') ||
        qf.selectedField.type.toLowerCase().includes('bit')
      ) {
        query = query + ' = ' + qf.option.toLocaleLowerCase().trim();
      } else {
        if (qf.option.toLocaleLowerCase() == 'greater than')
          query = query + ' > ' + qf.val.trim();
        else if (qf.option.toLocaleLowerCase() == 'less than')
          query = query + ' < ' + qf.val.trim();
        else if (
          qf.option.toLocaleLowerCase() == 'equals' ||
          qf.option.toLocaleLowerCase() == 'like' ||
          qf.option.toLocaleLowerCase().startsWith('in ')
        ) {
          if (qf.selectedField.type.toLowerCase().includes('char')) {
            if (qf.option.toLocaleLowerCase() == 'like') {
              query =
                query + " LIKE '%" + qf.val.trim().toLocaleLowerCase() + "%'";
            } else {
              let vals = qf.val.trim().toLocaleLowerCase().split('$');
              query = query + ' IN ( ';
              for (let i = 0; i < vals.length; i++) {
                if (i == 0) {
                  query = query + "'" + vals[i].trim() + "'";
                } else {
                  query = query + ", '" + vals[i].trim() + "'";
                }
              }
              query = query + ' )';
            }
          } else query = query + ' = ' + qf.val.trim();
        }
      }
    }
    return query;
  }

  formatDateForDb(date: string): string {
    const data = date.split('/');
    return data[2] + '-' + data[1] + '-' + data[0];
  }

  validateQuery(): boolean {
    var rv: boolean = this.queryFields.length > 0;
    this.queryFields.forEach((qf) => {
      if (rv && !qf.isFirst && qf.operator == undefined) rv = false;
      if (rv && qf.selectedTable == undefined) rv = false;
      if (qf.selectedField != undefined) {
        if (
          rv &&
          (qf.selectedField.type.toLowerCase().includes('date') ||
            qf.selectedField.type.toLowerCase().includes('time')) &&
          (qf.fromDate == undefined || qf.toDate == undefined) &&
          (qf.fromTime == undefined || qf.toTime == undefined)
        )
          rv = false;
        if (
          rv &&
          !(
            qf.selectedField.type.toLowerCase().includes('date') ||
            qf.selectedField.type.toLowerCase().includes('time')
          )
        ) {
          if (qf.option == undefined) rv = false;
          if (
            rv &&
            !(
              qf.selectedField.type.toLowerCase().includes('bool') ||
              qf.selectedField.type.toLowerCase().includes('bit')
            ) &&
            (qf.val == undefined ||
              qf.val == '' ||
              ((qf.selectedField.type.toLowerCase().includes('int') ||
                qf.selectedField.type.toLowerCase().includes('float') ||
                qf.selectedField.type.toLowerCase().includes('double')) &&
                isNaN(Number(qf.val))))
          )
            rv = false;
        }
      }
    });
    if (!rv) alert('Invalid Query!');
    return rv;
  }

  updateFields($event: DbTableDataForSelect) {
    var idx = this.querySelectFields.findIndex(
      (e) => e.tableData.tableName == $event.tableName
    );
    if (idx == -1) {
      var qsf = new QueySelectTreeComponent();
      qsf.tableData = $event;
      this.querySelectFields.push(qsf);
    }

    // remove irrelevant fields if table changed
    for (let index = 0; index < this.querySelectFields.length; index++) {
      let removeElement: boolean = true;
      this.queryFields.forEach((qf) => {
        if (
          qf.selectedTable == this.querySelectFields[index].tableData.tableName
        ) {
          removeElement = false;
        }
      });
      if (removeElement) {
        this.querySelectFields.splice(index--, 1);
      }
    }
  }

  addQueryComponent() {
    var qf = new QueryFieldComponent(this.dbTableService);
    qf.isFirst = this.queryFields.length == 0;
    qf.tables = this.tables;
    this.queryFields.push(qf);
  }

  removeQueryField(idx: number) {
    var numOfOccurrences = 0;
    var tableName = this.queryFields[idx].selectedTable;
    this.queryFields.forEach((qf) => {
      if (qf.selectedTable == tableName) numOfOccurrences++;
    });
    if (numOfOccurrences < 2) {
      for (var i = 0; i < this.querySelectFields.length; i++) {
        if (this.querySelectFields[i].tableData.tableName == tableName) {
          this.querySelectFields.splice(i--, 1);
        }
      }
    }
    if (idx > -1) {
      if (this.queryFields.length > idx + 1 && this.queryFields[idx].isFirst) {
        this.queryFields[idx + 1].isFirst = true;
      }
      this.queryFields.splice(idx, 1);
    }
  }

  trackById = (_index: number, item: any) => {
    return item.Id;
  };

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }
}

export interface DbTableField {
  name: string;
  type: string;
}

export interface DbTableDataForSelect {
  tableName: string;
  fields: DbTableField[];
}
