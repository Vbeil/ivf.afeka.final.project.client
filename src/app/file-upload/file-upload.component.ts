import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { DbTableService } from '../services/db-table.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit {
  data: any;
  tableName: string;
  fileAttr = 'Choose File';
  @ViewChild('inputFile') inputFile: ElementRef;
  isExcelOrCsvFile: boolean;
  isExcelFile: boolean;
  isLoadingResults: boolean = false;
  isOverride: boolean = false;
  constructor(private dbTableService: DbTableService) {}

  ngOnInit(): void {}

  onChange(evt) {
    const target: DataTransfer = <DataTransfer>evt.target;
    this.isExcelOrCsvFile = !!target.files[0].name.match(/(.xls|.xlsx|.csv)/);
    this.isExcelFile = !!target.files[0].name.match(/(.xls|.xlsx)/);
    if (target.files.length > 1) {
      this.inputFile.nativeElement.value = '';
      this.fileAttr = 'Choose File';
    }
    if (this.isExcelOrCsvFile) {
      this.isLoadingResults = true;
      this.fileAttr = target.files[0].name;
      this.tableName = this.normalizeTableName(
        target.files[0].name.substring(0, target.files[0].name.lastIndexOf('.'))
      );
      const reader: FileReader = new FileReader();

      reader.onload = (e: any) => {
        if (this.isExcelFile) {
          this.data = this.xslJSON(e.target.result);
        } else {
          this.data = this.csvJSON(e.target.result);
        }
      };

      if (this.isExcelFile) reader.readAsBinaryString(target.files[0]);
      else reader.readAsText(target.files[0]);

      reader.onloadend = (e) => {
        this.isLoadingResults = false;
      };
    } else {
      this.inputFile.nativeElement.value = '';
      this.fileAttr = 'Choose File';
    }
  }

  upload() {
    if (this.data != undefined) {
      this.isLoadingResults = true;
      this.dbTableService
        .upload(this.data, this.tableName, this.isOverride)
        .subscribe((e) => {
          this.isLoadingResults = false;
          alert('Data uploaded to the Database');
        });
    }
  }

  xslJSON(xsl) {
    const bstr: string = xsl;
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    //normalize header names
    const result = XLSX.utils.sheet_to_json(ws);
    result.forEach((obj) => this.normalizeKey(obj));
    return result;
  }

  csvJSON(csv) {
    var lines = csv.trim().split('\n');
    var result = [];
    var headers = lines[0].split(',');
    for (var i = 1; i < lines.length; i++) {
      var obj = {};
      var currentline = lines[i].split(',');
      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }
      result.push(obj);
    }
    result.forEach((obj) => this.normalizeKey(obj));
    return result;
  }

  normalizeKey(obj) {
    const keys = Object.keys(obj);

    keys.forEach((oldKey) => {
      var newKey = '';
      // break oldKey to words by delimiter
      var words = oldKey
        .replace('/', ' ')
        .split(/([A-Z][a-z]+)|[_]|[-]|[\s]|[\\]|[=]|[,]/);
      words.forEach((word) => {
        if (word && !/[\u0590-\u05FF]/.test(word))
          word = word.replace(/\W/g, '');
        if (word) {
          //if first word then toLower else
          if (newKey) {
            // not empty
            newKey +=
              word.charAt(0).toUpperCase() +
              word.substring(1).toLowerCase().trim();
          } else {
            newKey = word.toLowerCase().trim();
          }
        }
      });
      if (newKey != oldKey) {
        obj[newKey] = obj[oldKey];
        delete obj[oldKey];
      }
    });
  }

  normalizeTableName(fileName: string) {
    fileName = fileName.toLowerCase().replace('table', '');
    return fileName.replace(/[^A-Za-z]+/g, '');
  }

  onOverrideCheckboxChange() {
    this.isOverride = !this.isOverride;
  }

  clearDatabase() {
    var r = confirm('Are You Sure?');
    if (r == true) {
      this.isLoadingResults = true;
      this.dbTableService.clearDatabase().subscribe((e) => {
        this.isLoadingResults = false;
        alert('Database cleared');
      });
    }
  }
}

export class usedKeys {
  key: string;
  instances: number;
}
