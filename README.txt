Use vs code cli to:
	1. Install Angular: npm install -g @angular/cli
	2. Install Angular Material: ng add @angular/material
	3. Install google maps component: npm install @angular/google-maps
	4. Install ngx-translate component: npm install @ngx-translate/core --save
	5. Install mat-table-exporter: npm install --save mat-table-exporter
	6. build and run:
		6.1. ng serve 
		6.2. open http://localhost:4200/ 
		
* make sure you have nodeJs on your system